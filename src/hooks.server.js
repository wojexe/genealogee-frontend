import { i18n } from "$lib/i18n";

/** @type {import("@sveltejs/kit").Handle} */
export const handle = i18n.handle();
